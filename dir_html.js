const fs = require('fs');
const path_ = require('path');


let dirToHtml = {
    html_: "",
    get html() {
        return this.html_ ;
    },
    set html(str) {
        this.html_ += str + "<br/>";
    },
    walk_dir: function (path, counter = 1 ) {
        let files = fs.readdirSync(path, { withFileTypes: true})
        const dir_prefix = this.repeatStr('-', counter);
        const file_prefix = this.repeatStr('&nbsp;', counter*3);
        // console.log(`${dir_prefix}${path}`)
        //  html +=`${dir_prefix}${path}`
        this.html =`${dir_prefix}${path}`

        for(let i= 0 ; i< files.length ; i++ ) {
            let f = files[i];
            if(f.name === '.DS_Store') {
                continue;
            }
            const fullPath = path_.join(path, f.name);
            if (f.isFile()) {
                let urlPath = path_.join(path, encodeURIComponent(f.name));
                let url = this.createUrl(urlPath, f.name);
                // console.log(`${file_prefix}${url}`);
                this.html =`${file_prefix}${url}`

            } else if(f.isDirectory()){
                this.walk_dir(fullPath, counter + 1);
            }
        }
    },
    repeatStr: (str, n) => {
        return Array(n+1).join(str)
    },
    createUrl: (path, name) => {
        // let url = encodeURIComponent(path);
        return `<a href="${path}">${name}</a>`
    }
}


module.exports = dirToHtml

// dirToHtml.walk_dir('./videos');
//
// console.log(dirToHtml.html)


