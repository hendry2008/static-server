const fs = require('fs');
const path_ = require('path');

// async function print(path) {
//     const dir = fs.opendirSync(path);
//     for await (const dirent of dir) {
//         console.log(dirent.name);
//     }
// }

function print(path, counter) {
    fs.readdir(path, { withFileTypes: true}, (err, files) => {

        const dir_prefix = repeatStr('-', counter);
        const file_prefix = repeatStr(' ', counter);
        console.log(`${dir_prefix}${path}`)
        files.forEach((f) => {
            const fullPath = path_.join(path, f.name);

            if (f.isFile()) {
                console.log(`${file_prefix}${f.name}`);

            } else if(f.isDirectory()){
                // console.log(`${f.name} is dir `);
                print(fullPath, counter + 1);
            }
        });
    });
}

function repeatStr(str, n) {
    return Array(n+1).join(str)
}


print('./videos' , 1)
