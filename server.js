const express = require("express");
const dirToHtml = require("./dir_html")

const app = express();

const port = 9797;

app.use("/videos", express.static('videos'))
// 指定使用模板
app.set("view engine", "ejs");
app.set("views", "./views");

dirToHtml.walk_dir("./videos");
// 生成的html 为 dirToHtml.html
// console.log(dirToHtml.html);

app.get('/', (req, res) => {
    // res.send("hello, world!")
    res.render('template', {body: dirToHtml.html})
});


app.get("/test", (req, res) => {
    res.render("template", {username: "hendry"})
})


app.listen(port, () => {
    console.log(`Listening on port ${port}`)
});
